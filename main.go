package main

import (
	"context"
	"log"
	"time"

	"github.com/jaslife1/uniuri"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/node-service/proto/node"
)

func main() {
	srv := micro.NewService(

		micro.Name("node-service-cli"),
		micro.Version("latest"),
	)

	// Init will parse the command line flags.
	srv.Init()

	// Create a new client
	c := pb.NewNodeServiceClient("node-service", client.DefaultClient)

	// Get all stores in ground floor
	{
		id := uniuri.New()

		start := time.Now()
		log.Println("Get Stores In Floor")
		log.Println("RequestID: ", id)
		ctx := metadata.NewContext(context.Background(), map[string]string{
			"requestID": id,
		})
		duration := time.Now().Add(1 * time.Second)
		ctx, cancel := context.WithDeadline(ctx, duration)
		defer cancel()
		floorID := "5c72eaec1c9d4400006ed3c8"
		resp, err := c.GetNodesPerFloor(ctx, &pb.Node{FloorID: floorID, Type: "Store"})

		if err != nil {
			log.Panicf("Error: Did not get all the stores in floor with ID %s - %v", floorID, err)
		}

		log.Printf("Nodes: %v", resp.Nodes)

		elapsed := time.Since(start)
		log.Printf("Elapsed time: %s", elapsed)
	}

	// Get specific Node
	{
		id := uniuri.New()

		start := time.Now()
		log.Println("Get Specific Node")
		log.Println("RequestID: ", id)
		ctx := metadata.NewContext(context.Background(), map[string]string{
			"requestID": id,
		})
		duration := time.Now().Add(1 * time.Second)
		ctx, cancel := context.WithDeadline(ctx, duration)
		defer cancel()
		nodeID := "5c72eb431c9d4400006ed3cb"
		resp, err := c.GetNodeByID(ctx, &pb.Node{Id: nodeID})

		if err != nil {
			log.Panicf("Error: Did not get node with ID: %s - %v", nodeID, err)
		}

		log.Printf("Node: %v", resp.Node)

		elapsed := time.Since(start)
		log.Printf("Elapsed time: %s", elapsed)
	}

	// Get all Nodes
	{
		id := uniuri.New()

		start := time.Now()
		log.Println("Requesting all nodes")
		log.Println("RequestID: ", id)
		ctx := metadata.NewContext(context.Background(), map[string]string{
			"requestID": id,
		})
		duration := time.Now().Add(1 * time.Second)
		ctx, cancel := context.WithDeadline(ctx, duration)
		defer cancel()
		resp, err := c.GetAllNodes(ctx, &pb.Node{})

		if err != nil {
			log.Panicf("Error: Did not get all the  nodes - %v", err)
		}

		log.Printf("Nodes: %v", resp.Nodes)

		elapsed := time.Since(start)
		log.Printf("Elapsed time: %s", elapsed)
	}
}
